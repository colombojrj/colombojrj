"""
WSGI config for colombojrj project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

# Default
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "colombojrj.settings")
#application = get_wsgi_application()

# Whitenoise
from whitenoise.django import DjangoWhiteNoise
application = get_wsgi_application()
application = DjangoWhiteNoise(application)