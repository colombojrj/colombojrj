# -*- coding: utf-8 -*-

from django import forms

class FormularioComentario(forms.Form):
    autor = forms.CharField(label='Nome', max_length=100)
    conteudo = forms.CharField(label=u'Comentário', 
                               max_length=1000, 
                               widget=forms.Textarea())
    
