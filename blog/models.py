# -*- coding: utf-8 -*-

from django.db import models
#from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

class Categoria(models.Model):
  titulo = models.CharField(max_length = 100)
  slug   = models.SlugField(max_length = 100)
            
  def __unicode__(self):
    return u'%s'%(self.titulo)

class Artigo(models.Model):
  titulo          = models.CharField(max_length = 100)
  #conteudo        = RichTextField()
  conteudo        = RichTextUploadingField()
  publicado       = models.BooleanField(default = False)
  data_publicacao = models.DateTimeField('Data', auto_now_add = True)
  autor           = models.CharField(max_length = 100)
  slug            = models.SlugField(max_length = 100, unique = True)
  categoria       = models.ForeignKey(Categoria)

  def __unicode__(self):
    return self.titulo

class Comentario(models.Model):
  conteudo        = models.CharField(max_length = 1000)
  artigo          = models.ForeignKey(Artigo)
  data_publicacao = models.DateTimeField('Data', auto_now_add = True)
  autor           = models.CharField(max_length = 100)
  publicado       = models.BooleanField(default = False)
  
  def __unicode__(self):
    return u'%s'%(self.conteudo)
