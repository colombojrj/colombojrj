# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Categoria, Artigo, Comentario

class AdminComentario(admin.ModelAdmin):
    list_display = ('autor', 'artigo', 'publicado',)
    list_editable = ('publicado',)

admin.site.register(Categoria)
admin.site.register(Artigo)
admin.site.register(Comentario, AdminComentario)
