from django.conf.urls import url
from . import views

urlpatterns = [
  # exemplo: /
  url(r'^$', views.index, name='index'),
  # exemplo: /artigo/15
  url(r'^artigo/(?P<id_artigo>[0-9]+)/$', views.ver_artigo, name='ver_artigo'),
  # exemplo: /categoria/1
  url(r'^categoria/(?P<id_categoria>[0-9]+)/$', views.ver_artigos_categoria,
      name='ver_categoria'),
]