# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from .models import Artigo, Categoria, Comentario
from .forms import FormularioComentario

def index(request):
  # Carrega os últimos 5 artigos
  artigos = Artigo.objects.order_by('-data_publicacao')[:5]
  
  # Carrega todas as categorias
  categorias = Categoria.objects.all()
  
  # Organiza esses dados em um dicionários e os envia para visualização
  context = {"artigos"    : artigos,
             "categorias" : categorias}
  return render(request, 'blog/artigos.html', context)

def ver_artigo(request, id_artigo):
  # Carrega todas as categorias
  categorias = Categoria.objects.all()  
  context = {"categorias" : categorias}

  # Carrega o artigo selecionado
  artigo_selecionado = get_object_or_404(Artigo, pk=id_artigo)
  context["artigo"] = artigo_selecionado

  # Por enquanto, assume-se que nenhum formulário foi recebido
  context["comentario_recebido"] = False
  
  # "Pega" o formulário preenchido enviado pelo leitor
  if request.method == 'POST':
    formulario = FormularioComentario(request.POST)
    
    # Se o formulário é válido, então "pega" os campos preenchidos
    if formulario.is_valid():
      handle = Comentario(conteudo  = formulario.cleaned_data['conteudo'], 
                          autor     = formulario.cleaned_data['autor'],
                          artigo    = artigo_selecionado,
                          publicado = False)
      handle.save()
      context["comentario_recebido"] = True
    else:
      context["formulario"] = formulario
      
  else:
    # Carrega os comentários de um determinado artigo
    comentarios = artigo_selecionado.comentario_set.all()
    
    # Cria um formulário em branco para o leitor inserir um comentário
    formulario = FormularioComentario()
    
    # Organiza esses dados num dicionário
    context["comentarios"] = comentarios.order_by('-data_publicacao')[:5]
    context["formulario"]  = formulario
  
  return render(request, 'blog/artigo.html', context)

def ver_artigos_categoria(request, id_categoria):
  # Carrega a categoria escolhida pelo usuário
  categoria = Categoria.objects.get(pk=id_categoria)
  
  # Carrega todos os artigos dessa categoria
  artigos = categoria.artigo_set.order_by('-data_publicacao')
  
  # Carrega todas as categorias
  categorias = Categoria.objects.all()
  
  # Organiza esses dados num dicionário
  context = {"artigos"    : artigos,
             "categorias" : categorias}
             
  return render(request, 'blog/artigos.html', context)


