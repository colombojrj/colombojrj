# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Escolha, Pergunta, Enquete

class PerguntaEmLinha(admin.TabularInline):
    model = Pergunta
    extra = 1

class EscolhaEmLinha(admin.TabularInline):
    model = Escolha
    extra = 1

class AdminEnquete(admin.ModelAdmin):
    search_fields = ['titulo']
    inlines = [PerguntaEmLinha]
    list_display = ('titulo', 'data_publicacao')
    
class AdminPergunta(admin.ModelAdmin):
    inlines = [EscolhaEmLinha]
    search_fields = ['texto_pergunta']


admin.site.register(Enquete, AdminEnquete)
admin.site.register(Pergunta, AdminPergunta)
