# -*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm
from .models import Enquete, Pergunta, Escolha

class FormularioEnquete(ModelForm):
    class Meta:
        model = Enquete
        fields = []
    
    def __init__(self, *args, **kwargs):
        enquete = kwargs.pop('enquete', '')
        super(FormularioEnquete, self).__init__(*args, **kwargs)
        for pergunta in enquete.pergunta_set.all():
            escolhas = pergunta.escolha_set.all()
            item = pergunta.texto_pergunta
            self.fields[item] = forms.ModelChoiceField(widget=forms.RadioSelect(attrs={'id': 'value'}), 
                                                       queryset=escolhas, 
                                                       empty_label=None,)
            self.fields[item].label = pergunta.texto_pergunta
    
    def save(self, enquete):
        for item in self.data:
            if item != 'csrfmiddlewaretoken':
                escolha_votada = Escolha.objects.get(pk=self.data[item])
                escolha_votada.votos = escolha_votada.votos + 1
                escolha_votada.save()
                


