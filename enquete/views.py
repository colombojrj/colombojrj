# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import Enquete, Pergunta, Escolha
from .forms import FormularioEnquete

def index(request):
    enquetes = Enquete.objects.order_by('-data_publicacao')[:5]

    context = {'enquetes': enquetes}
    
    return render(request, 'enquete/index.html', context)

def detalhes(request, id_enquete):
    # Pega a enquete que o internauta solicitou
    enquete = get_object_or_404(Enquete, pk=id_enquete)
    
    formulario = FormularioEnquete(enquete=enquete)
    
    context = {'enquete': enquete, 'formulario' : formulario}
    
    return render(request, 'enquete/detalhes.html', context)

def votar(request, id_enquete):
    enquete = get_object_or_404(Enquete, pk=id_enquete)
    
    formulario = FormularioEnquete(request.POST, enquete=enquete)
    
    if formulario.is_valid():
        formulario.save(enquete)
    
    
    #selected_choice = p.escolha_set.get(pk=request.POST['escolha'])
    #selected_choice.votos += 1
    #selected_choice.save()
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    #return HttpResponseRedirect(reverse('enquete:resultado', args=(p.id,)))
    return HttpResponse('Obrigado pela sua participação!')

        
def resultado(request, id_enquete):
    enquete = get_object_or_404(Enquete, pk=id_enquete)
    return render(request, 'enquete/resultado.html', {'enquete': enquete})