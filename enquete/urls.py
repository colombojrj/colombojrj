# -*- coding: utf-8 -*-

from django.conf.urls import url

from . import views

urlpatterns = [
  # ex: /enquete
  url(r'^$', views.index, name='index'),
  # ex: /enquete/5
  url(r'^(?P<id_enquete>[0-9]+)/$', views.detalhes, name='detalhes'),
  # ex: /enquete/5/votar
  url(r'^(?P<id_enquete>[0-9]+)/votar/$', views.votar, name='votar'),
  # ex: /enquete/5/resultado
  url(r'^(?P<id_enquete>[0-9]+)/resultado/$',views.resultado,name='resultado'),
]