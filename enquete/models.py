# -*- coding: utf-8 -*-

from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

class Enquete(models.Model):
    titulo          = models.CharField(max_length=100, default='')
    preambulo       = RichTextUploadingField()
    slug            = models.SlugField(max_length=100, default='')
    data_publicacao = models.DateField('Data')
    
    def __str__(self):
        return self.titulo

class Pergunta(models.Model):
    formulario      = models.ForeignKey(Enquete)
    texto_pergunta  = RichTextUploadingField()
    slug            = models.SlugField(max_length=100)
    
    def __str__(self):
      return self.slug

class Escolha(models.Model):
    pergunta = models.ForeignKey(Pergunta)
    texto_escolha = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)
    
    def __unicode__(self):
        return u'%s'%(self.texto_escolha)
    

    
    